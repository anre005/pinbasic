# Installation

Installation of the package should be straightforward. 
Since this package is not available on CRAN yet you have to use the `devtools` package or clone this repo
to install a development version.

## Install via `devtools` 
To install the development version of the package use the following line of code

`devtools::install_bitbucket("anre005/pinbasic/pkg")`

## Cloning the Repo

URL for cloning the Repo:

[https://anre005@bitbucket.org/anre005/pinbasic.git](https://anre005@bitbucket.org/anre005/pinbasic.git)

# Building the Vignette

To build the vignette several other R packages are needed:

* `knitr`
* [`printr`](https://github.com/yihui/printr)
* `xtable`
* `tikzDevice`
* `highr`

All of these can be found on CRAN except for the [`printr`](https://github.com/yihui/printr) package. 
If a package is missing it will be installed on-the-fly the first time you try to build the package vignette.

XeTex is used as compiler for the Rnw file of the vignette. Ensure that XeTeX/XeLaTeX is available on your system.

The *vignettes* directory contains a *Makefile* which compiles the tex file three times and runs *biber* twice to ensure 
that there are no undefinded references and the bibliography is complete.

So the bigger the vignette gets the longer the vignette needs to compile.

## Fonts
The fonts used in the package vignette are:

* **MinionPro** as main font (shipped with Adobe Reader)
* **MyriadPro** as sans font (shipped with Adobe Reader)
* **Source Code Pro** as mono font
* ~~**Neo-Euler** as math font with some symbols from **Asana-Math** which are not available in **Neo-Euler**~~
* **MinionPro** TeX package loaded with onlymath option. Package sources and instructions how to install the fonts
  can be found on [Github](https://github.com/sebschub/FontPro)