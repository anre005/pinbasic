# Makefile for generating R packages.
# 2011 Andrew Redd
#
# Assumes Makefile is in a folder where package contents are in a subfolder pkg.
# Roxygen uses the roxygen2 package, and will run automatically on check and all.

PKG_VERSION=$(shell grep -i ^version DESCRIPTION | cut -d : -d \  -f 2)
PKG_NAME=$(shell grep -i ^package DESCRIPTION | cut -d : -d \  -f 2)

R_FILES := $(wildcard R/*.R)
SRC_FILES := $(wildcard src/*) $(addprefix src/, $(COPY_SRC))
PKG_FILES := DESCRIPTION NAMESPACE $(R_FILES) $(SRC_FILES)
MAN_FILES := man

.PHONY: tarball install check clean build

tarball: $(PKG_NAME)_$(PKG_VERSION).tar.gz
$(PKG_NAME)_$(PKG_VERSION).tar.gz: $(PKG_FILES)
	R CMD build --no-build-vignettes ../pinbasic
	mv $(PKG_NAME)_$(PKG_VERSION).tar.gz ../

check: $(PKG_NAME)_$(PKG_VERSION).tar.gz
	R CMD check ../$(PKG_NAME)_$(PKG_VERSION).tar.gz

install: $(PKG_NAME)_$(PKG_VERSION).tar.gz
	R CMD INSTALL --byte-compile ../$(PKG_NAME)_$(PKG_VERSION).tar.gz

helpfiles: $(R_FILES)
	Rscript -e "devtools:::document()"

manual: $(MAN_FILES)
	R CMD Rd2pdf --no-preview --output=../pinbasic_manual.pdf --force ../pinbasic/

clean:
	-rm -f $(PKG_NAME)_*.tar.gz
	-rm -r -f $(PKG_NAME).Rcheck
	-rm -r -f pkg/man/*
	-rm -r -f pkg/NAMESPACE
.PHONY: list
list:
	@echo "R files:"
	@echo $(R_FILES)
	@echo "Source files:"
	@echo $(SRC_FILES)
